//////////////////////////////////////////////////////
//  droneSimulator.h
//
//  Created on: Mar 25, 2014
//      Author: jl.sanchez
//
//  Last modification on: Mar 26, 2014
//      Author: jl.sanchez
//
//////////////////////////////////////////////////////



#ifndef DRONE_SIMULATOR_H
#define DRONE_SIMULATOR_H


#include <iostream>
#include <string>
#include <vector>
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


#include <cmath>

#include "opencv2/opencv.hpp"




#include "pose.h"


#include "arenaSimulator.h"



//XML parser
#include "pugixml.hpp"






/////////////////////////////////////////
// Class Drone
//
//   Description
//
/////////////////////////////////////////
class Drone
{
//Sector del trapecio de percepcion
protected:
    float minDistanceDetection; //m
    float maxDistanceDetection; //m
    float sensor_horizontal_view_direction; //rad
    float sensor_horizontal_field_of_view; //rad

public:
    //Input in: m, m, deg, deg
    int setDetectionConfig(float minDistanceDetection, float maxDistanceDetection, float sensor_horizontal_view_direction, float sensor_horizontal_field_of_view);
    int setDetectionConfig(std::string configFile);


    //Pose of the Drone wrt World
protected:
    SL::Pose PoseDrone_wrt_World;


    //World
protected:
    StaticWorld* TheWorld;


    //Drone
public:
    Drone();
    Drone(StaticWorld* TheWorld);
    ~Drone();


public:
    int setWorld(StaticWorld* TheWorld);


public:
    int setPose(const SL::Pose& PoseDrone_wrt_World);


public:
    int getDetectedGridPoints(std::vector<cv::Point3f>& detectedGridPoints);
    int getDroneNavData(std::vector<double>& positionDroneNavData, std::vector<double>& attitudeDroneNavData) { return PoseDrone_wrt_World.getDroneNavData( positionDroneNavData, attitudeDroneNavData); }
    float getminDistanceDetection() { return minDistanceDetection; }
    float getmaxDistanceDetection() { return maxDistanceDetection; }
    float getsensor_horizontal_view_direction() { return sensor_horizontal_view_direction; }
    float getsensor_horizontal_field_of_view() { return minDistanceDetection; }

};






#endif
