//////////////////////////////////////////////////////
//  MulticameraRobotTrackeriRobotCVROSModule.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef MULTICAMERA_ROBOT_TRACKER_IROBOTCV_ROS_MODULE_H
#define MULTICAMERA_ROBOT_TRACKER_IROBOTCV_ROS_MODULE_H

#define FORGET_PREVIOUS_IROBOT_FEEDBACK


//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"



//OpenCV
#include <opencv2/opencv.hpp>



#include "droneSimulator.h"
#include "arenaSimulator.h"



//Subscribers: dronePoseStamped
#include "droneMsgsROS/dronePoseStamped.h"
//Subscribers: relative to iRobot's robotPose...
#include "droneMsgsROS/robotPose.h"
#include "droneMsgsROS/robotPoseVector.h"
#include "droneMsgsROS/robotPoseStamped.h"
#include "droneMsgsROS/robotPoseStampedVector.h"

//Publishers
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/points3DStamped.h"



const double MULTICAMERA_ROBOT_TRACKER_IROBOTCV_RATE = 10.0;



/////////////////////////////////////////
// Class MulticameraRobotTrackeriRobotCVROSModule
//
//   Description
//
/////////////////////////////////////////
class MulticameraRobotTrackeriRobotCVROSModule : public Module
{	
    //Main classes
protected:
    //WORLD
    Arena IarcArena;

    //Drone
    Drone MyDrone;

    //exchange info
    std::vector<cv::Point3f> gridIntersections;


public:
    std::string sensorConfig;
    int setSensorConfig(std::string sensorConfig);


public:
    int setTopicConfigs(std::string dronePoseSubsTopicName, std::string iRobotCVPublTopicName);



    //subscribers
protected:
    //Topic name
    std::string dronePoseSubsTopicName;
    //Subscriber
    ros::Subscriber dronePoseSubs;
public:
    void dronePoseCallback(const droneMsgsROS::dronePoseStamped::ConstPtr& msg);

    //subscribers
protected:
    //Subscriber
    ros::Subscriber iRobotsSimPoseSubs_back;
    ros::Subscriber iRobotsSimPoseSubs_bottom;
    ros::Subscriber iRobotsSimPoseSubs_left;
    ros::Subscriber iRobotsSimPoseSubs_front;
    ros::Subscriber iRobotsSimPoseSubs_right;
    void iRobotsSimPoseCallback(const droneMsgsROS::robotPoseVector::ConstPtr& msg);
    droneMsgsROS::robotPoseStampedVector irobot_pose_wrt_droneGMR_vector_msg;

    //publishers
protected:
    //Topic name
    std::string iRobotCVPublTopicName;
    //Publisher
    ros::Publisher iRobotCVPublisher;
protected:
    bool publishIRobotsCV();




public:
    MulticameraRobotTrackeriRobotCVROSModule();
    ~MulticameraRobotTrackeriRobotCVROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};








#endif
