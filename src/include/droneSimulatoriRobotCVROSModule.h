//////////////////////////////////////////////////////
//  droneSimulatoriRobotCVROSModule.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef DRONE_SIMULATOR_IROBOTCV_ROS_MODULE_H
#define DRONE_SIMULATOR_IROBOTCV_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"



//OpenCV
#include <opencv2/opencv.hpp>



#include "droneSimulator.h"
#include "arenaSimulator.h"



//Subscribers: dronePoseStamped
#include "droneMsgsROS/dronePoseStamped.h"
//Subscribers: relative to iRobot's robotPose...
#include "droneMsgsROS/robotPose.h"
#include "droneMsgsROS/robotPoseVector.h"
#include "droneMsgsROS/robotPoseStamped.h"
#include "droneMsgsROS/robotPoseStampedVector.h"

//Publishers
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/points3DStamped.h"



const double DRONE_SIMULATOR_IROBOTCV_RATE = 10.0;



/////////////////////////////////////////
// Class DroneSimulatoriRobotCVROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneSimulatoriRobotCVROSModule : public Module
{	
    //Main classes
protected:
    //WORLD
    Arena IarcArena;

    //Drone
    Drone MyDrone;

    //exchange info
    std::vector<cv::Point3f> gridIntersections;


public:
    std::string sensorConfig;
    int setSensorConfig(std::string sensorConfig);


public:
    int setTopicConfigs(std::string dronePoseSubsTopicName, std::string iRobotCVPublTopicName);



    //subscribers
protected:
    //Topic name
    std::string dronePoseSubsTopicName;
    //Subscriber
    ros::Subscriber dronePoseSubs;
public:
    void dronePoseCallback(const droneMsgsROS::dronePoseStamped::ConstPtr& msg);

    //subscribers
protected:
    //Subscriber
    ros::Subscriber iRobotsSimPoseSubs;
    void iRobotsSimPoseCallback(const droneMsgsROS::robotPose::ConstPtr& msg);
    droneMsgsROS::robotPoseVector robot_pose_wrt_world_vector;
//    droneMsgsROS::robotPoseStampedVector robot_pose_wrt_world_vector;

    //publishers
protected:
    //Topic name
    std::string iRobotCVPublTopicName;
    //Publisher
    ros::Publisher iRobotCVPublisher;
protected:
    bool publishIRobotsCV();




public:
    DroneSimulatoriRobotCVROSModule();
    ~DroneSimulatoriRobotCVROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};








#endif
