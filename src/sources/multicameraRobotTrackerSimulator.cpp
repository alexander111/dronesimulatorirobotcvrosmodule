//////////////////////////////////////////////////////
//  MulticameraRobotTrackeriRobotCVROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "multicameraRobotTrackerSimulator.h"


using namespace std;

MulticameraRobotTrackeriRobotCVROSModule::MulticameraRobotTrackeriRobotCVROSModule() : Module(droneModule::active,MULTICAMERA_ROBOT_TRACKER_IROBOTCV_RATE)
{
    irobot_pose_wrt_droneGMR_vector_msg.robot_pose_vector.clear();
    return;
}


MulticameraRobotTrackeriRobotCVROSModule::~MulticameraRobotTrackeriRobotCVROSModule()
{
	close();
	return;
}

int MulticameraRobotTrackeriRobotCVROSModule::setSensorConfig(std::string sensorConfig)
{
    this->sensorConfig=sensorConfig;
    return 1;
}

int MulticameraRobotTrackeriRobotCVROSModule::setTopicConfigs(std::string dronePoseSubsTopicName, std::string gridIntersectionsPublTopicName)
{
    this->dronePoseSubsTopicName=dronePoseSubsTopicName;
    this->iRobotCVPublTopicName=gridIntersectionsPublTopicName;
    return 1;
}



bool MulticameraRobotTrackeriRobotCVROSModule::init()
{
    Module::init();


    //Do stuff

    //Set World
    MyDrone.setWorld(&IarcArena);

    //Configure sensor
    MyDrone.setDetectionConfig(sensorConfig);


    //end
    return true;
}


void MulticameraRobotTrackeriRobotCVROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    Module::open(nIn);


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }



    //// TOPICS
    //Subscribers
    //Commands to test
    //rostopic pub -1 /drone0/EstimatedPose_droneGMR_wrt_GFF droneMsgsROS/dronePoseStamped -- '{header: auto, pose:[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "a", "b", "c"]}'
    //rostopic pub -1 /drone0/EstimatedPose_droneGMR_wrt_GFF droneMsgsROS/dronePoseStamped -- '{header: auto, pose:[1.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0, "a", "b", "c"]}'
    dronePoseSubs             = n.subscribe(dronePoseSubsTopicName, 1, &MulticameraRobotTrackeriRobotCVROSModule::dronePoseCallback,      this);
    //                                       perception/back/robots_detection
    iRobotsSimPoseSubs_back   = n.subscribe("perception/back/robots_detection",   1, &MulticameraRobotTrackeriRobotCVROSModule::iRobotsSimPoseCallback, this);
    iRobotsSimPoseSubs_bottom = n.subscribe("perception/bottom/robots_detection", 1, &MulticameraRobotTrackeriRobotCVROSModule::iRobotsSimPoseCallback, this);
    iRobotsSimPoseSubs_left   = n.subscribe("perception/left/robots_detection",   1, &MulticameraRobotTrackeriRobotCVROSModule::iRobotsSimPoseCallback, this);
    iRobotsSimPoseSubs_front  = n.subscribe("perception/front/robots_detection",  1, &MulticameraRobotTrackeriRobotCVROSModule::iRobotsSimPoseCallback, this);
    iRobotsSimPoseSubs_right  = n.subscribe("perception/right/robots_detection",  1, &MulticameraRobotTrackeriRobotCVROSModule::iRobotsSimPoseCallback, this);


    //Publishers
    iRobotCVPublisher=n.advertise<droneMsgsROS::robotPoseStampedVector>(iRobotCVPublTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;

	
	//End
	return;
}


void MulticameraRobotTrackeriRobotCVROSModule::close()
{
    Module::close();



    //Do stuff

    return;
}


bool MulticameraRobotTrackeriRobotCVROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool MulticameraRobotTrackeriRobotCVROSModule::startVal()
{
    //Do stuff

    //End
    return Module::startVal();
}


bool MulticameraRobotTrackeriRobotCVROSModule::stopVal()
{
    //Do stuff

    return Module::stopVal();
}


bool MulticameraRobotTrackeriRobotCVROSModule::run()
{
    if(!Module::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    //Publish
    if(!publishIRobotsCV())
    {
        cout<<"error publishing"<<endl;
        return false;
    }

#ifdef FORGET_PREVIOUS_IROBOT_FEEDBACK
    irobot_pose_wrt_droneGMR_vector_msg.robot_pose_vector.clear();
#endif
    return true;
}

void MulticameraRobotTrackeriRobotCVROSModule::dronePoseCallback(const droneMsgsROS::dronePoseStamped::ConstPtr& msg)
{
    //Receive
    SL::Pose PoseDrone_wrt_world;
    std::vector<double> tvec(3); //X, Y, Z
    std::vector<double> navAngles(3); //Yaw, Pitch, Roll


    tvec[0]=msg->pose.x;
    tvec[1]=msg->pose.y;
    tvec[2]=msg->pose.z;

    navAngles[0]=msg->pose.yaw;
    navAngles[1]=msg->pose.pitch;
    navAngles[2]=msg->pose.roll;


    PoseDrone_wrt_world.setDroneNavData(tvec,navAngles);

    //Set pose of the drone wrt world
    MyDrone.setPose(PoseDrone_wrt_world);


    return;
}

void MulticameraRobotTrackeriRobotCVROSModule::iRobotsSimPoseCallback(const droneMsgsROS::robotPoseVector::ConstPtr &msg)
{
    std::vector<droneMsgsROS::robotPose>::const_iterator it_msg;
    for ( it_msg  = msg->robot_pose_vector.begin();
          it_msg != msg->robot_pose_vector.end();
          it_msg++) {

        // check whether this iRobot is already in robot_pose_wrt_world_vector to update it or append it.
        droneMsgsROS::robotPoseStamped this_irobot;
        this_irobot.header.stamp = msg->header.stamp;
        this_irobot.x   = it_msg->x;
        this_irobot.y   = it_msg->y;
        this_irobot.z   = it_msg->z;
        this_irobot.theta        = it_msg->theta;
        this_irobot.id_Robot     = it_msg->id_Robot;
        this_irobot.Robot_Type   = it_msg->Robot_Type;

        bool this_irobot_is_listed = false;
        std::vector<droneMsgsROS::robotPoseStamped>::iterator it;
        for ( it  = irobot_pose_wrt_droneGMR_vector_msg.robot_pose_vector.begin();
              it != irobot_pose_wrt_droneGMR_vector_msg.robot_pose_vector.end();
              it++) {
            if ((*it).id_Robot == this_irobot.id_Robot) {
                this_irobot_is_listed = true;
                break;
            }
        }

        // update or append this_irobot to the robot_pose_wrt_world_vector
        if ( this_irobot_is_listed ) {
            (*it) = this_irobot;
        } else {
            irobot_pose_wrt_droneGMR_vector_msg.robot_pose_vector.push_back(this_irobot);
        }
    }
}


bool MulticameraRobotTrackeriRobotCVROSModule::publishIRobotsCV()
{
    if(droneModuleOpened==false)
        return false;

    //Publish
    iRobotCVPublisher.publish(irobot_pose_wrt_droneGMR_vector_msg);


    return true;

}


