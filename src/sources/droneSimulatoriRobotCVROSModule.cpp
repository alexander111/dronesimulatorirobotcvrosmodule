//////////////////////////////////////////////////////
//  droneSimulatoriRobotCVROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneSimulatoriRobotCVROSModule.h"



using namespace std;




DroneSimulatoriRobotCVROSModule::DroneSimulatoriRobotCVROSModule() : Module(droneModule::active,DRONE_SIMULATOR_IROBOTCV_RATE)
{

    return;
}


DroneSimulatoriRobotCVROSModule::~DroneSimulatoriRobotCVROSModule()
{
	close();
	return;
}

int DroneSimulatoriRobotCVROSModule::setSensorConfig(std::string sensorConfig)
{
    this->sensorConfig=sensorConfig;
    return 1;
}

int DroneSimulatoriRobotCVROSModule::setTopicConfigs(std::string dronePoseSubsTopicName, std::string gridIntersectionsPublTopicName)
{
    this->dronePoseSubsTopicName=dronePoseSubsTopicName;
    this->iRobotCVPublTopicName=gridIntersectionsPublTopicName;
    return 1;
}



bool DroneSimulatoriRobotCVROSModule::init()
{
    Module::init();


    //Do stuff

    //Set World
    MyDrone.setWorld(&IarcArena);

    //Configure sensor
    MyDrone.setDetectionConfig(sensorConfig);


    //end
    return true;
}


void DroneSimulatoriRobotCVROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    Module::open(nIn);


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }



    //// TOPICS
    //Subscribers
    //Commands to test
    //rostopic pub -1 /drone0/EstimatedPose_droneGMR_wrt_GFF droneMsgsROS/dronePoseStamped -- '{header: auto, pose:[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "a", "b", "c"]}'
    //rostopic pub -1 /drone0/EstimatedPose_droneGMR_wrt_GFF droneMsgsROS/dronePoseStamped -- '{header: auto, pose:[1.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0, "a", "b", "c"]}'
    dronePoseSubs      = n.subscribe(dronePoseSubsTopicName,  1, &DroneSimulatoriRobotCVROSModule::dronePoseCallback,      this);
    iRobotsSimPoseSubs = n.subscribe("chatter",              50, &DroneSimulatoriRobotCVROSModule::iRobotsSimPoseCallback, this);


    //Publishers
    iRobotCVPublisher=n.advertise<droneMsgsROS::robotPoseVector>(iRobotCVPublTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;

	
	//End
	return;
}


void DroneSimulatoriRobotCVROSModule::close()
{
    Module::close();



    //Do stuff

    return;
}


bool DroneSimulatoriRobotCVROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool DroneSimulatoriRobotCVROSModule::startVal()
{
    //Do stuff

    //End
    return Module::startVal();
}


bool DroneSimulatoriRobotCVROSModule::stopVal()
{
    //Do stuff

    return Module::stopVal();
}


bool DroneSimulatoriRobotCVROSModule::run()
{
    if(!Module::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    //Publish
    if(!publishIRobotsCV())
    {
        cout<<"error publishing"<<endl;
        return false;
    }

    

    return true;
}

void DroneSimulatoriRobotCVROSModule::dronePoseCallback(const droneMsgsROS::dronePoseStamped::ConstPtr& msg)
{
    //Receive
    SL::Pose PoseDrone_wrt_world;
    std::vector<double> tvec(3); //X, Y, Z
    std::vector<double> navAngles(3); //Yaw, Pitch, Roll


    tvec[0]=msg->pose.x;
    tvec[1]=msg->pose.y;
    tvec[2]=msg->pose.z;

    navAngles[0]=msg->pose.yaw;
    navAngles[1]=msg->pose.pitch;
    navAngles[2]=msg->pose.roll;


    PoseDrone_wrt_world.setDroneNavData(tvec,navAngles);

    //Set pose of the drone wrt world
    MyDrone.setPose(PoseDrone_wrt_world);


    return;
}

void DroneSimulatoriRobotCVROSModule::iRobotsSimPoseCallback(const droneMsgsROS::robotPose::ConstPtr &msg)
{


    // check whether this iRobot is already in robot_pose_wrt_world_vector to update it or append it.
//    droneMsgsROS::robotPoseStamped this_irobot;
    droneMsgsROS::robotPose this_irobot;
//    this_irobot.header.stamp = ros::Time::now();
    this_irobot.x   = msg->x;
    this_irobot.y   = msg->y;
    this_irobot.z   = msg->z;
    this_irobot.theta        = -(*msg).theta + 3.1415; // JP: no se xq hay que hacer esto...
    this_irobot.id_Robot     = msg->id_Robot;
    this_irobot.Robot_Type   = msg->Robot_Type;

    bool this_irobot_is_listed = false;
//    std::vector<droneMsgsROS::robotPoseStamped>::iterator it;
    std::vector<droneMsgsROS::robotPose>::iterator it;
    for ( it  = robot_pose_wrt_world_vector.robot_pose_vector.begin();
          it != robot_pose_wrt_world_vector.robot_pose_vector.end();
          it++) {
        if ((*it).id_Robot == this_irobot.id_Robot) {
            this_irobot_is_listed = true;
            break;
        }
    }

    // update or append this_irobot to the robot_pose_wrt_world_vector
    if ( this_irobot_is_listed ) {
        (*it) = this_irobot;
    } else {
        robot_pose_wrt_world_vector.robot_pose_vector.push_back(this_irobot);
    }
}


bool DroneSimulatoriRobotCVROSModule::publishIRobotsCV()
{
    if(droneModuleOpened==false)
        return false;

//    droneMsgsROS::robotPoseStampedVector irobot_pose_wrt_droneGMR_vector_msg;
    droneMsgsROS::robotPoseVector irobot_pose_wrt_droneGMR_vector_msg;
    irobot_pose_wrt_droneGMR_vector_msg.header.stamp = ros::Time::now();

    std::vector<double> dronePosition; //x, y, z
    std::vector<double> droneAttitude; //yaw, pitch, roll
    if(!MyDrone.getDroneNavData(dronePosition,droneAttitude))
    {
        //cout<<"error or no dronePose set"<<endl;
        return 0;
    }
    float minDistanceDetection = MyDrone.getminDistanceDetection();
    float maxDistanceDetection = MyDrone.getmaxDistanceDetection();
    float sensor_horizontal_view_direction = MyDrone.getsensor_horizontal_view_direction();
    float sensor_horizontal_field_of_view = MyDrone.getsensor_horizontal_field_of_view();
//    std::vector<droneMsgsROS::robotPoseStamped>::const_iterator it;
    std::vector<droneMsgsROS::robotPose>::const_iterator it;
    for ( it  = robot_pose_wrt_world_vector.robot_pose_vector.begin();
          it != robot_pose_wrt_world_vector.robot_pose_vector.end();
          it++) {
//        droneMsgsROS::robotPoseStamped this_irobot;
        droneMsgsROS::robotPose this_irobot;
//        this_irobot.header.stamp = (*it).header.stamp;
        this_irobot.id_Robot     = (*it).id_Robot;
        this_irobot.Robot_Type   = (*it).Robot_Type;

        double yaw = droneAttitude[0];
        double lx, ly, lz, lgx, lgy, lgz;
        lx = it->x - dronePosition[0];
        ly = it->y - dronePosition[1];
        lz = it->z - dronePosition[2];
        lgx =  cos(yaw)*lx + sin(yaw)*ly;
        lgy = -sin(yaw)*lx + cos(yaw)*ly;
        lgz =  lz;

        //Distance
        double distance = sqrt( pow(lgx,2) + pow(lgy,2) );
        //Angle
        double ux, uy;
        ux = cos(sensor_horizontal_view_direction);
        uy = sin(sensor_horizontal_view_direction);
        double dot_product   = (lgx * ux + lgy * uy);
        double cross_product = (lgx * uy - lgy * ux);
        double angle         = atan2( cross_product/distance, dot_product/distance);
        //Check
        if( (distance<=maxDistanceDetection && distance>=minDistanceDetection) && (fabs(angle)<=sensor_horizontal_field_of_view/2.0) )
        {
            this_irobot.x = lgx;
            this_irobot.y = lgy;
            this_irobot.z = lgz;
            this_irobot.theta = (*it).theta - yaw;
            this_irobot.theta = atan2( sin(this_irobot.theta), cos(this_irobot.theta));

            irobot_pose_wrt_droneGMR_vector_msg.robot_pose_vector.push_back(this_irobot);
        }

    }


    //Publish
    iRobotCVPublisher.publish(irobot_pose_wrt_droneGMR_vector_msg);


    return true;

}


