#include "arenaSimulator.h"


using namespace std;




StaticWorld::StaticWorld() :
    referenceSystem3D_Origin(1),
    referenceSystem3D(3)
{
        //Origin
        referenceSystem3D_Origin[0].x=0.0;
        referenceSystem3D_Origin[0].y=0.0;
        referenceSystem3D_Origin[0].z=0.0;

        //Vectors

        referenceSystem3D[0].x=1;

        referenceSystem3D[1].y=1;

        referenceSystem3D[2].z=1;


    return;
}

StaticWorld::~StaticWorld()
{

    return;
}

int StaticWorld::getReferenceSystem3D(std::vector<cv::Point3d>& referenceSystem3D_Origin, std::vector<cv::Point3d>& referenceSystem3D)
{
    referenceSystem3D_Origin=this->referenceSystem3D_Origin;
    referenceSystem3D=this->referenceSystem3D;

    return 1;
}


int StaticWorld::setPoints3D(const std::vector<cv::Point3d> &points3D)
{
    this->points3D=points3D;
    return 1;
}

int StaticWorld::getPoints3D(std::vector<cv::Point3d>& points3D)
{
    points3D=this->points3D;
    return 1;
}


int StaticWorld::setLines3D(const std::vector<Line<cv::Point3d> > &lines3D)
{
    this->lines3D=lines3D;

    return 1;
}
int StaticWorld::getLines3D(std::vector< Line<cv::Point3d>  >& lines3D)
{
    lines3D=this->lines3D;

    return 1;
}








Arena::Arena()
{

    //Grid points
    cv::Point3d OnePoint3d;
    for(unsigned int i=0;i<=20;i++)
    {
        for(unsigned int j=0;j<=20;j++)
        {
            //Sistema de referencia RS: origen a la derecha abajo
            OnePoint3d.x= (-1.0) * i*1.0;
            OnePoint3d.y= (+1.0) * j*1.0;
            OnePoint3d.z=0.0;
            this->points3D.push_back(OnePoint3d);

            //cout<<OnePoint3d<<endl;
        }
    }


    //Grid lines
    Line<cv::Point3d>  OneLine3D;
    cv::Point3d Point1, Point2;
    //Horizontal
    for(unsigned int i=0;i<=20;i++)
    {
        Point1.x=i*1;
        Point1.y=0;
        Point1.z=0;

        Point2.x=i*1;
        Point2.y=20;
        Point2.z=0;

        OneLine3D.setPoints(Point1,Point2);

        lines3D.push_back(OneLine3D);
    }
    //Vertical
    for(unsigned int i=0;i<=20;i++)
    {
        Point1.x=0;
        Point1.y=i*1;
        Point1.z=0;

        Point2.x=20;
        Point2.y=i*1;
        Point2.z=0;

        OneLine3D.setPoints(Point1,Point2);

        lines3D.push_back(OneLine3D);
    }


    return;
}

Arena::~Arena()
{

    return;
}



