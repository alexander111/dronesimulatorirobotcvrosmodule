

#include "droneSimulator.h"


using namespace std;


Drone::Drone() :
    TheWorld(0)
{

    return;
}


Drone::Drone(StaticWorld* TheWorld) :
    TheWorld(0)
{
    setWorld(TheWorld);

    return;
}


Drone::~Drone()
{

    return;
}

int Drone::setWorld(StaticWorld* TheWorld)
{
    this->TheWorld=TheWorld;
    return 1;
}



int Drone::setPose(const SL::Pose &DronePose)
{
    //Pose of the drone
    this->PoseDrone_wrt_World=DronePose;


    return 1;
}

int Drone::setDetectionConfig(float minDistanceDetection, float maxDistanceDetection, float sensor_horizontal_view_direction, float sensor_horizontal_field_of_view)
{
    this->minDistanceDetection=minDistanceDetection;
    this->maxDistanceDetection=maxDistanceDetection;
    this->sensor_horizontal_view_direction=sensor_horizontal_view_direction*M_PI/180.0;
    this->sensor_horizontal_field_of_view=sensor_horizontal_field_of_view*M_PI/180.0;

    cout<<"minDistanceDetection="<<minDistanceDetection<<endl;
    cout<<"maxDistanceDetection="<<maxDistanceDetection<<endl;
    cout<<"sensor_horizontal_view_direction="<<sensor_horizontal_view_direction<<endl;
    cout<<"sensor_horizontal_field_of_view="<<sensor_horizontal_field_of_view<<endl;

    return 1;
}

int Drone::setDetectionConfig(std::string configFile)
{
    //
    cout<<"configFile="<<configFile<<endl;

    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        cout<<"error opening xml file"<<endl;
        return 0;
    }

    std::string readingValue;
    pugi::xml_node simulator_grid = doc.child("simulator_grid");

    float minDistanceDetection, maxDistanceDetection, sensor_horizontal_view_direction, sensor_horizontal_field_of_view;

    //minDistanceDetection
    readingValue=simulator_grid.child_value("minDistanceDetection");
    istringstream convertminDistanceDetection(readingValue);
    convertminDistanceDetection>>minDistanceDetection;

    //maxDistanceDetection
    readingValue=simulator_grid.child_value("maxDistanceDetection");
    istringstream convertmaxDistanceDetection(readingValue);
    convertmaxDistanceDetection>>maxDistanceDetection;

    //sensor_horizontal_view_direction
    readingValue=simulator_grid.child_value("sensor_horizontal_view_direction");
    istringstream convertsensor_horizontal_view_direction(readingValue);
    convertsensor_horizontal_view_direction>>sensor_horizontal_view_direction;

    //minDistanceDetection
    readingValue=simulator_grid.child_value("sensor_horizontal_field_of_view");
    istringstream convertsensor_horizontal_field_of_view(readingValue);
    convertsensor_horizontal_field_of_view>>sensor_horizontal_field_of_view;


    setDetectionConfig(minDistanceDetection,maxDistanceDetection,sensor_horizontal_view_direction,sensor_horizontal_field_of_view);

    return 1;
}


int Drone::getDetectedGridPoints(std::vector<cv::Point3f>& detectedGridPoints)
{
    //Clean
    detectedGridPoints.resize(0);

    //Mapa
    std::vector<cv::Point3d> points3DMap;
    TheWorld->getPoints3D(points3DMap);

    std::vector<double> dronePosition; //x, y, z
    std::vector<double> droneAttitude; //yaw, pitch, roll
    if(!PoseDrone_wrt_World.getDroneNavData(dronePosition,droneAttitude))
    {
        //cout<<"error or no dronePose set"<<endl;
        return 0;
    }

    for(std::vector<cv::Point3d>::iterator it=points3DMap.begin(); it!=points3DMap.end();++it)
    {
        double yaw = droneAttitude[0];
        double lx, ly, lz, lgx, lgy, lgz;
        lx = it->x - dronePosition[0];
        ly = it->y - dronePosition[1];
        lz = it->z - dronePosition[2];
        lgx =  cos(yaw)*lx + sin(yaw)*ly;
        lgy = -sin(yaw)*lx + cos(yaw)*ly;
        lgz =  lz;

        //Distance
        double distance = sqrt( pow(lgx,2) + pow(lgy,2) );
        //Angle
        double ux, uy;
        ux = cos(sensor_horizontal_view_direction);
        uy = sin(sensor_horizontal_view_direction);
        double dot_product   = (lgx * ux + lgy * uy);
        double cross_product = (lgx * uy - lgy * ux);
        double angle         = atan2( cross_product/distance, dot_product/distance);
        //Check
        if( (distance<=maxDistanceDetection && distance>=minDistanceDetection) && (fabs(angle)<=sensor_horizontal_field_of_view/2.0) )
        {
            cv::Point3f pointInArena;
            pointInArena.x = lgx;
            pointInArena.y = lgy;
            pointInArena.z = lgz;

            detectedGridPoints.push_back(pointInArena);
        }

    }


    //return
    return 1;
}



